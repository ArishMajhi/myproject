package np.com.infodev.network;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public class NetworkClient {

    public static NetworkInterface networkInterface;


    public static NetworkInterface getNetworkInterface() {
        if (networkInterface == null) {
            Retrofit retroClient = new Retrofit.Builder()
                    .baseUrl("")
                    .client(provideOkHttpCleint())
                    .addConverter(String.class, new ToStringConverter())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            networkInterface = retroClient.create(NetworkInterface.class);
        }
        return networkInterface;
    }

    public static OkHttpClient provideOkHttpCleint() {
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setConnectTimeout(10, TimeUnit.SECONDS);
        okHttpClient.setReadTimeout(30, TimeUnit.SECONDS);
        okHttpClient.interceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                return chain.proceed(chain.request());
            }
        });
        return okHttpClient;
    }


}
