package np.com.infodev.network;


import np.com.infodev.cmis.module.login.pojo.LoginResponse;
import retrofit.Call;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

public interface NetworkInterface {

    @FormUrlEncoded
    @POST("login")
    Call<LoginResponse> login(@Field("USERNAME") String username,
                                  @Field("USERTYPE") String userType,
                                  @Field("SIGNATURE") String signature);
}
