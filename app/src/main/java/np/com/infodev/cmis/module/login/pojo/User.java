package np.com.infodev.cmis.module.login.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class User implements Serializable {

    @Expose
    @SerializedName("USERID")
    String mUsername;
    @Expose
    @SerializedName("NAME")
    String mName;
    @Expose
    @SerializedName("LOGIN_NAME")
    String mLoginName;

    public User(String mUsername, String mName, String mLoginName) {
        this.mUsername = mUsername;
        this.mName = mName;
        this.mLoginName = mLoginName;
    }

    public String getmUsername() {
        return mUsername;
    }

    public void setmUsername(String mUsername) {
        this.mUsername = mUsername;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmLoginName() {
        return mLoginName;
    }

    public void setmLoginName(String mLoginName) {
        this.mLoginName = mLoginName;
    }
}
