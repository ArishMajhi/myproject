package np.com.infodev.cmis.base;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import butterknife.ButterKnife;
import np.com.infodev.cmis.app.AppRuntimeInitializer;


/**
 * Created by PC on 1/3/2018.
 */

public abstract class BaseActivity extends AppCompatActivity {

    protected boolean isActivityLive = false;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentView());
        ButterKnife.bind(this);

        onViewReady(savedInstanceState, getIntent());
    }

    protected abstract int getContentView();


    @CallSuper
    protected void onViewReady(Bundle savedInstanceState, Intent intent) {
    }

    protected void showProgressDialog(String message) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(BaseActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        }
        progressDialog.setMessage(message);
        progressDialog.setCancelable(true);
        progressDialog.show();
    }

    protected void hideProgressDialog() {
        if (isActivityLive) {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }

    protected void showError(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        AppRuntimeInitializer.INSTANCE.dealOnRequestPermissionsResult(this, requestCode, permissions, grantResults);
    }


    @Override
    public void onStart() {
        super.onStart();
        isActivityLive = true;
    }

    @Override
    public void onStop() {
        super.onStop();
        isActivityLive = false;
    }


}
