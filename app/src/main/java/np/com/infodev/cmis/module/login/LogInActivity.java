package np.com.infodev.cmis.module.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import butterknife.BindView;
import np.com.infodev.cmis.MainActivity;
import np.com.infodev.cmis.R;
import np.com.infodev.cmis.base.BaseActivity;
import np.com.infodev.cmis.module.login.presenter.LoginIntercace;
import np.com.infodev.cmis.module.login.presenter.LoginPresenterImpl;

public class LogInActivity extends BaseActivity implements View.OnClickListener, LoginIntercace.ILoginView {
    /*defination section*/
    LoginIntercace.LoginPresenter loginPresenter;


    /*view id*/
    @BindView(R.id.activity_login_username)
    EditText mUserName;
    @BindView(R.id.activity_login_password)
    EditText mPassword;
    @BindView(R.id.activity_login_submit_button)
    Button mSubmitButton;

    @Override
    protected int getContentView() {
        return R.layout.activity_log_in;
    }

    @Override
    protected void onViewReady(Bundle savedInstanceState, Intent intent) {
        super.onViewReady(savedInstanceState, intent);

        mSubmitButton.setOnClickListener(this);
        loginPresenter = new LoginPresenterImpl(this, LogInActivity.this);
    }

    @Override
    public void onClick(View v) {
        makeTransition();

//        if (TextUtils.isEmpty(mUserName.getText())) {
//            mUserName.setError("Username Required");
//            mUserName.requestFocus();
//        } else if (TextUtils.isEmpty(mPassword.getText())) {
//            mPassword.setError("Password Required");
//            mPassword.requestFocus();
//        } else {
//            loginPresenter.requestLogin(mUserName.getText().toString(), mPassword.getText().toString());
//        }
    }

    @Override
    public void showErrorMessage(String message) {
        showError(message);
    }

    @Override
    public void hideDailog() {
        hideProgressDialog();
    }

    @Override
    public void showDialog(String message) {
        showProgressDialog(message);
    }

    @Override
    public void makeTransition() {
        this.startActivity(new Intent(this, MainActivity.class));
        this.finish();
    }
}
