package np.com.infodev.cmis.module.login.presenter;

import android.app.Activity;

import np.com.infodev.cmis.base.ConnectivityReceiver;
import np.com.infodev.cmis.module.login.extras.SHA256;
import np.com.infodev.cmis.module.login.interactor.LoginInteractor;
import np.com.infodev.cmis.module.login.pojo.LoginResponse;
import np.com.infodev.network.NetworkClient;
import retrofit.Callback;
import retrofit.Response;

public class LoginPresenterImpl implements LoginIntercace.LoginPresenter {
    LoginIntercace.ILoginView iLoginView;
    Activity activity;
    LoginInteractor loginInteractor;


    public LoginPresenterImpl(LoginIntercace.ILoginView iLoginView, Activity activity){
        this.iLoginView = iLoginView;
        this.activity = activity;
        loginInteractor = new LoginInteractor(activity);
    }
    @Override
    public void requestLogin(String username, String password) {

        if(ConnectivityReceiver.isConnected()){
            iLoginView.showDialog("Processing,Please wait...");
            NetworkClient.getNetworkInterface().login(
                    username,
                    getUserType("1"),
                    getEncrypted(getUserType("1") + username + password)).enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Response<LoginResponse> response) {
                    if(response.isSuccess()){
                        if (response.body() != null) {
                            if (response.body().getStatus_code().equals("0")) {
                                if (response.body().getData() != null) {
                                    iLoginView.hideDailog();
                                    iLoginView.showErrorMessage(response.body().getMessage());
                                    iLoginView.makeTransition();
                                }else {
                                    iLoginView.hideDailog();
                                    iLoginView.showErrorMessage(response.body().getMessage());
                                }
                            } else {
                                iLoginView.hideDailog();
                                iLoginView.showErrorMessage(response.body().getMessage());
                            }

                        } else {
                            iLoginView.hideDailog();
                            iLoginView.showErrorMessage(response.message());

                        }
                    }else {
                        iLoginView.showErrorMessage(response.message());
                        iLoginView.hideDailog();

                    }


                }

                @Override
                public void onFailure(Throwable t) {

                }
            });
        }else {
            iLoginView.showErrorMessage("No Internet Connection");
        }


    }

    private String getEncrypted(String s) {
        return SHA256.getEncryptedValue(s);
    }

    private String getUserType(String s) {

        if (s.equalsIgnoreCase("Staff")) {
            return "1";
        } else {
            return "2";
        }
    }
}
