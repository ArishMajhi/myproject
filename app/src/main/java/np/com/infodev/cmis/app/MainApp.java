package np.com.infodev.cmis.app;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;


import np.com.infodev.cmis.BuildConfig;
import np.com.infodev.cmis.base.ConnectivityReceiver;

/**
 * Created by PC on 1/2/2018.
 */

public class MainApp extends Application {
    private static MainApp mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.APPLICATION_ID.equals(getCurProcessName(this))) {
            AppRuntimeInitializer.INSTANCE.initRuntime(this);
        }
        mInstance = this;
    }


    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public static synchronized MainApp getInstance() {
        return mInstance;
    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }


    private String getCurProcessName(Context context) {
        int pid = android.os.Process.myPid();
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningAppProcessInfo appProcess : activityManager.getRunningAppProcesses()) {
            if (appProcess.pid == pid) {
                return appProcess.processName;
            }
        }
        return "";
    }
}
