package np.com.infodev.cmis.module.login.presenter;

public interface LoginIntercace {
    interface  LoginPresenter{
        void requestLogin(String username, String password);
    }
    interface  ILoginView{
        void showErrorMessage(String message);

        void hideDailog();

        void showDialog(String message);

        void makeTransition();
    }
}
