package np.com.infodev.cmis.app;

import android.app.Activity;
import android.content.Context;

import java.lang.ref.WeakReference;
import java.util.LinkedList;

/**
 * Created by afon on 2017/1/24.
 */

public class AppRuntime {

    public static Context sContext;
    public static LinkedList<Activity> sActivities = new LinkedList<>();
    public static WeakReference<Activity> sActivityStops = new WeakReference<>(null);

    public static String serverVersion = "1.0";
    public static String appVersion = "1.0.0";
}
