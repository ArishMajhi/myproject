package np.com.infodev.cmis;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import butterknife.BindView;
import butterknife.ButterKnife;
import np.com.infodev.cmis.base.BaseActivity;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.activity_main_btn_download)
    View mDownloadView;

    @BindView(R.id.activity_main_btn_registration)
    View mRegistrationView;

    @BindView(R.id.activity_main_btn_upload)
    View mUploadView;

    @BindView(R.id.activity_main_btn_edit)
    View mEditView;

    @Override
    protected int getContentView() {
        return R.layout.activity_main;
    }

    @Override
    protected void onViewReady(Bundle savedInstanceState, Intent intent) {
        super.onViewReady(savedInstanceState, intent);
        mDownloadView.setOnClickListener(this);
        mRegistrationView.setOnClickListener(this);
        mUploadView.setOnClickListener(this);
        mEditView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activity_main_btn_download:

                break;

            case R.id.activity_main_btn_registration:

                break;
            case R.id.activity_main_btn_upload:

                break;

            case R.id.activity_main_btn_edit:

                break;
            default:

                break;
        }
    }
}
