package np.com.infodev.cmis.module.login.pojo;

import com.google.gson.annotations.Expose;

public class LoginResponse {
    @Expose
    String status;

    @Expose
    String message;

    @Expose
    String token;

    @Expose
    String status_code;

    @Expose
    User data;

    public LoginResponse(String status, String message, String token, String status_code) {
        this.status = status;
        this.message = message;
        this.token = token;
        this.status_code = status_code;
    }


    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public User getData() {
        return data;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }
}
